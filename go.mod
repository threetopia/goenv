module gitlab.com/threetopia/goenv

go 1.19

require github.com/joho/godotenv v1.4.0
