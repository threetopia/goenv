package goenv

import "testing"

func TestGoEnv_LoadDotEnv(t *testing.T) {
	goEnv := New()
	err := goEnv.LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
}

func TestGoEnv_GetString(t *testing.T) {
	goEnv := New()
	err := goEnv.LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	str := goEnv.GetString("TEST_GET_STRING", "")
	if str != "Loaded Correctly" {
		t.Errorf("Not Match (%s is wrong value)", str)
	}
}

func TestGoEnv_GetSlice(t *testing.T) {
	goEnv := New()
	err := goEnv.LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	slc := [3]string{"ValueOne", "ValueTwo", "ValueThree"}
	slice := goEnv.GetSlice("TEST_GET_SLICE")
	for k, v := range slc {
		if slice[k] != v {
			t.Errorf("Not Match (%s is wrong value for %s)", slice[k], v)
		}
	}

	goEnv.SetDelimiter(",")
	slice = goEnv.GetSlice("TEST_GET_SLICE_DELIMITER")
	for k, v := range slc {
		if slice[k] != v {
			t.Errorf("Not Match (%s is wrong value for %s)", slice[k], v)
		}
	}
}

func TestGoEnv_GetInt(t *testing.T) {
	goEnv := New()
	err := goEnv.LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	integer := goEnv.GetInt("TEST_GET_NUMBER", 0)
	if integer != 1234567890 {
		t.Errorf("Not Match (%d is wrong value)", integer)
	}
}

func TestGoEnv_GetInt64(t *testing.T) {
	goEnv := New()
	err := goEnv.LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	integer := goEnv.GetInt64("TEST_GET_NUMBER", 0)
	if integer != int64(1234567890) {
		t.Errorf("Not Match (%d is wrong value)", integer)
	}
}

func TestGoEnv_GetFloat32(t *testing.T) {
	goEnv := New()
	err := goEnv.LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	f64 := goEnv.GetFloat32("TEST_GET_NUMBER", 0)
	if f64 != float32(1234567890) {
		t.Errorf("Not Match (%f is wrong value)", f64)
	}
}

func TestGoEnv_GetFloat64(t *testing.T) {
	goEnv := New()
	err := goEnv.LoadDotEnv()
	if err != nil {
		t.Errorf("Error (%s)", err.Error())
	}
	f64 := goEnv.GetFloat64("TEST_GET_NUMBER", 0)
	if f64 != float64(1234567890) {
		t.Errorf("Not Match (%f is wrong value)", f64)
	}
}
