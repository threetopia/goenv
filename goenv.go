package goenv

import (
	"os"
	"strconv"
	"strings"

	"github.com/joho/godotenv"
)

const delimiter string = ";"

type GoEnv struct {
	delimiter string
}

func New() *GoEnv {
	return &GoEnv{
		delimiter: delimiter,
	}
}

func (ge *GoEnv) LoadDotEnv() error {
	err := godotenv.Load()
	if err != nil {
		return err
	}
	return nil
}

func (ge *GoEnv) SetDelimiter(delimiter string) {
	ge.delimiter = delimiter
}

func (ge *GoEnv) GetString(path string, predefined string) string {
	res := os.Getenv(path)
	if res != "" {
		return res
	}
	return predefined
}

func (ge *GoEnv) GetSlice(path string) []string {
	res := make([]string, 0)
	getString := ge.GetString(path, "")
	if getString == "" {
		return res
	}
	return strings.Split(getString, ge.delimiter)
}

func (ge *GoEnv) GetInt(path string, predefined int) int {
	res := os.Getenv(path)
	if res != "" {
		ires, err := strconv.Atoi(res)
		if err == nil {
			return ires
		}
	}
	return predefined
}

func (ge *GoEnv) GetInt64(path string, predefined int64) int64 {
	res := os.Getenv(path)
	if res != "" {
		ires, err := strconv.ParseInt(res, 10, 64)
		if err == nil {
			return ires
		}
	}
	return predefined
}

func (ge *GoEnv) GetFloat32(path string, predefined float32) float32 {
	res := os.Getenv(path)
	if res != "" {
		if f64, err := strconv.ParseFloat(res, 32); err == nil {
			return float32(f64)
		}
	}
	return predefined
}

func (ge *GoEnv) GetFloat64(path string, predefined float64) float64 {
	res := os.Getenv(path)
	if res != "" {
		if f64, err := strconv.ParseFloat(res, 64); err == nil {
			return f64
		}
	}
	return predefined
}
